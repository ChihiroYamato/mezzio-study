<?php

declare(strict_types=1);

namespace App\Handler\AmoCRM;

use Laminas\Diactoros\Response\HtmlResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class AuthHandler implements RequestHandlerInterface
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $apiClient = new \AmoCRM\Client\AmoCRMApiClient($_ENV['AMO_CLIENT_ID'], $_ENV['AMO_CLIENT_SECRET'], $_ENV['AMO_REDIRECT_URL']);

        if (! isset($request->getQueryParams()['code'])) {
            $state = bin2hex(random_bytes(16));
            $_SESSION['oauth2state'] = $state;

            $authorizationUrl = $apiClient->getOAuthClient()->getAuthorizeUrl([
                'state' => $state,
                'mode' => 'post_message',
            ]);

            header('Location: ' . $authorizationUrl);
            die;
        } elseif (empty($request->getQueryParams()['state']) || empty($_SESSION['oauth2state']) || ($request->getQueryParams()['state'] !== $_SESSION['oauth2state'])) {
            unset($_SESSION['oauth2state']);

            return new HtmlResponse('<div>invalid state</div>', 400);
        }

        try {
            if (isset($request->getQueryParams()['referer'])) {
                $apiClient->setAccountBaseDomain($request->getQueryParams()['referer']);
            }

            $accessToken = $apiClient->getOAuthClient()->getAccessTokenByCode($request->getQueryParams()['code']);

            $tokenJson = json_encode([
                'accessToken' => $accessToken->getToken(),
                'refreshToken' => $accessToken->getRefreshToken(),
                'expires' => $accessToken->getExpires(),
                'baseDomain' => $apiClient->getAccountBaseDomain(),
            ], JSON_FORCE_OBJECT);

            if (! $accessToken->hasExpired()) {
                file_put_contents((new \App\ConfigProvider())->getSecretFolderPath() . '/access_token.json', $tokenJson);
            }

            return new HtmlResponse(sprintf('<h1>Hello %s</h1>', $apiClient->getAccountBaseDomain()));
        } catch (\Exception $e) {
            return new HtmlResponse(sprintf('<div>%s</div>', (string) $e), 400);
        }
    }
}
